library(randomForest)

RF_model <- randomForest(x=insurDataTrain %>% select(-y),
                         y=(insurDataTrain %>% .$y %>% as_factor()), 
                         ntree=100, 
                         proximity=TRUE, 
                         oob.prox=FALSE)

plot(RF_model)

save(RF_model, file="RF_model.rda")