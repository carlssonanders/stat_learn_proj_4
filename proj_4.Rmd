---
title: "Project 4"
author: "Anders Carlsson and Leo Gumpert"
date: "15 jan 2020"
output: pdf_document
fig_width: 6 
fig_height: 10
---

```{r setup, echo=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r, echo=FALSE, include=FALSE}
# Swith to true to run compare_models.r and make all
# the graphs, takes a little longer.
library(knitr)
boolean_make_graphs <- TRUE

if(boolean_make_graphs){
  source("compare_models.r",encoding = "UTF-8")
}

```

# About the data set and the task

The chosen data set was the "Insurance Company Benchmark (COIL 2000) Data Set"
https://archive.ics.uci.edu/ml/datasets/Insurance+Company+Benchmark+(COIL+2000)
As we work as actuaries, we were interested in using a data set which had some connection to insurance, and this one seemed to be the only to do so. 

The data set was released as a part of the CoIL Challenge 2000. The goal was to predict which customers are most likely to buy a caravan insurance policy. The data consists of 85 predictors, which are categorical variables which describe where the person live, religious belief, family, educational level, relationship, number of cars, income, employment info and a lot of variables associated with the area where the person lived. The 0-1 response variable is whether or not the person owns a caravan insurance policy. The training data set, consisting of 5000 observations, included the response variable, whereas the test set did not. The prediction task of the CoIL challenge was to select a subset of the test set (800 out of 4000) which are the most likely owners of a caravan insurance policy. The task is then scored by counting the number of policy owners in the subset.

The response variable was quite imbalanced, with approximately only 6% policy owners in the training set. 

# Model choice
The two models that were chosen were a logistic ridge regression model and a neural network. Both models outputs a number that is transformed through a sigmoid function into a probability. If we had chosen models whose output did not include class probabilities, the class imbalance would have meant that the models had predicted 0 on all of the observations in the test set which would have rendered them useless. 

## The logistic ridge regression model
In the logistic ridge regression model all 85 predictors were chosen without any higher order terms or intersecting terms. The lambda parameter was chosen using 10-fold cross-validation. The parameter $\lambda_{min}$ that minimized the cross-validation estimate of the test error were calculated. The selected parameter $\lambda_{reg}$ represents the most regularized model for which the estimated test error was within the error bars of $\lambda_{min}$. 

```{r, echo=FALSE}
if(boolean_make_graphs){
  plot(glmnet_model)
}
```

If one looks at the size of the coefficients, one can see that some of the factors seems more important than others

```{r, echo=FALSE}
if(boolean_make_graphs){
  kable(top_important_coeff)
}
```

## The neural network model
In the neural network model we experimented with different numbers of hidden layers and number of nodes. The input layer was decided by the size of the data which was 85 predictors. The number of output layers were one and a sigmoid function was used to make a probability. As a loss function binomial deviance was used, just like in the logistic ridge regression model example. For some reason there was a factor of 2 difference in the loss functions between the calculated loss of the ridge regression and the neural network model, therefore the validation error in the ridge regression graph above seems to be around double that of the neural network whereas in fact they are quite similar.


```{r, echo=FALSE}
if(boolean_make_graphs){
  plot(history1)
}
```

## Implementation and tuning
The logistic ridge regression model was fitted using the glmnet package in R and the neural network model was fitted using the keras package in R with the tensorflow backend.

While the ridge regression was done with no additional fine tuning of the model, some effort was applied to choose a good performing neural network. All models were tested using 100 epochs, which seemed to train the model well enough. In the table the estimation of the test error is given along the attributes of the 6 variants of the neural networks that were tested.

```{r, echo=FALSE}
if(boolean_make_graphs){
  kable(summary_nn)
}
```

The first model consisting of one hidden layer with 100 nodes giving a estimated test error of 0.227. Increasing the number of hidden layers to two gave a slightly lower error of 0.222. Decreasing the number of nodes to 50 gave a higher error, increasing the number of nodes to 1000 gave a still lower error. Increasing the number of hidden layers to two 1000 nodes layers gave a little increase to the test error. Therefore we choose model 5 as our neural network model. All the estimations of the test error were done with data from the training set. In the next step where we compare the neural network with the ridge regression, we use data from the actual test set to compare these two models.

## Performance on the test set
Using the ridge regression model and the model 5 of the neural network model, we compared how many of the respective models subset of 800 out of the test set of 4000 that resultad in finding a caravan owner. The result was that the ridge regression model "found" 109 caravan owners while the neural network model found 115. The winning contribution to the CoIL Challenge found 121 caravan owners. 


# Discussion

We consider the performance from the two models on the test set to be quite similar. Which one to choose is not clear, both seem to be good choices. Choosing the model comes down to taste and what you want out of the model. Maybe the ridge regression model has a bit better potential to be interpretable, even though we chose the neural network model that only had one hidden layer.

One possible improvement to the tuning of the neural network would be to refrain from fixing the number of epochs at start. Instead, one could consider using the method of early stopping. With this method, one sets aside a hold out validation set, and stops training the model when the loss starts to increase for the validation set. 

We also did not apply any "manual" feature selection or reduction. The coefficient table of the ridge regression, among other things, suggest that one might have been able to achieve a good result by throwing away most or all of the zip-code derived predictors from the start, since the predictors containing information on other insurance policies owned might be the most predictive. 

# References 

van der Putten et. al. (2000) CoIL Challenge 2000 Tasks and Results: Predicting and Explaining
Caravan Policy Ownership
http://liacs.leidenuniv.nl/~puttenpwhvander/library/cc2000/PUTTEN~1.pdf 

# Appendice

## get_data.r

```{r engine='bash', comment='', echo=FALSE}
cat get_data.r
```

## prep_data.r

```{r engine='bash', comment='', echo=FALSE}
cat prep_data.r
```

## ridge.r

```{r engine='bash', comment='', echo=FALSE}
cat ridge.r
```

## neural_network.r

```{r engine='bash', comment='', echo=FALSE}
cat neural_network.r
```

## random_forest.r

```{r engine='bash', comment='', echo=FALSE}
cat random_forest.r
```

## compare_models.r

```{r engine='bash', comment='', echo=FALSE}
cat compare_models.r
```
