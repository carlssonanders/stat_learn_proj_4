library(glmnet)

# Takes 90 sec with alpha=1 
# 10 sec with alpha = 0
# on 2012 macbook
set.seed(1)
system.time({
  glmnet_model <- cv.glmnet(x = X_train_matrix, 
                            y = y_train_frame, 
                            # alpha=1 => lasso
                            # alpha=0 => ridge
                            alpha = 0, 
                            family="binomial")
})

save(glmnet_model, file="glmnet_model.rda")

coeff_ridge <- coefficients(glmnet_model)
coeff_ridge <- as.matrix(coeff_ridge)

coeff_ridge <- tibble(Nr=names(coeff_ridge[,1]),
                      value=unname(coeff_ridge[,1]),
                      abs_value=abs(value))

coeff_ridge <- coeff_ridge %>% mutate(Nr=substring(Nr,2))

coeff_ridge <- merge(coeff_ridge,
                             (insurDict %>% 
                                select("Nr","Description")), by="Nr")

coeff_ridge <- coeff_ridge %>% 
                mutate(Description=str_replace(Description,
                                               "Number of","Num of"))

coeff_ridge <- coeff_ridge %>% arrange(desc(abs_value), .by_group = FALSE)
top_important_coeff <- coeff_ridge %>% mutate(row_number=1:n()) %>%
  filter(row_number<=30) %>%
  select(Description, value)

top_important_coeff <- top_important_coeff %>%
  mutate(Description=substring(Description,1,70))

save(top_important_coeff, file="top_important_coeff.rda")
