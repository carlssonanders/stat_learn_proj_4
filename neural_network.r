library(keras)

set.seed(1)
nn_model1 <- keras_model_sequential() 
nn_model2 <- keras_model_sequential() 
nn_model3 <- keras_model_sequential() 
nn_model4 <- keras_model_sequential() 
nn_model5 <- keras_model_sequential() 
nn_model6 <- keras_model_sequential() 
nn_model7 <- keras_model_sequential() 
nn_model8 <- keras_model_sequential() 
nn_model9 <- keras_model_sequential() 

num_epochs <- 100

##########################################
nn_model1 %>% 
  layer_dense(units = 100, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.05) %>% 
  layer_dense(units = 2, activation = 'softmax')

########################################## An extra hidden layer
nn_model2 %>% 
  layer_dense(units = 100, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.05) %>% 
  layer_dense(units = 100, activation = 'relu') %>%
  layer_dropout(rate = 0.1) %>%
  layer_dense(units = 2, activation = 'softmax')

########################################## Reduce number of nodes
nn_model3 %>% 
  layer_dense(units = 50, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.05) %>% 
  layer_dense(units = 2, activation = 'softmax')

########################################## Another extra layer
nn_model4 %>% 
  layer_dense(units = 100, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.05) %>% 
  layer_dense(units = 100, activation = 'relu') %>%
  layer_dropout(rate = 0.1) %>%
  layer_dense(units = 100, activation = 'relu') %>%
  layer_dropout(rate = 0.1) %>%
  layer_dense(units = 2, activation = 'softmax')

########################################## A bigger hidden layer
nn_model5 %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.05) %>% 
  layer_dense(units = 2, activation = 'softmax')

########################################## An extra hidden layer, Winner
nn_model6 %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.05) %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.01) %>% 
  layer_dense(units = 2, activation = 'softmax')

########################################## Another extra hidden layer
nn_model7 %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.05) %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.01) %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.01) %>% 
  layer_dense(units = 2, activation = 'softmax')

########################################## Reduce the dropout rate
nn_model8 %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.025) %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.005) %>% 
  layer_dense(units = 2, activation = 'softmax')

########################################## Increase the dropout rate
nn_model9 %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.1) %>% 
  layer_dense(units = 1000, activation = 'relu', input_shape = c(85)) %>% 
  layer_dropout(rate = 0.02) %>% 
  layer_dense(units = 2, activation = 'softmax')


nn_model1 %>% compile(
  loss = 'binary_crossentropy',
  optimizer = optimizer_sgd(lr = 0.001, momentum = 0, 
                            decay = 0, nesterov = FALSE,
                            clipnorm = NULL, clipvalue = NULL)
)

nn_model2 %>% compile(
  loss = 'binary_crossentropy',
  optimizer = optimizer_sgd(lr = 0.001, momentum = 0, 
                            decay = 0, nesterov = FALSE,
                            clipnorm = NULL, clipvalue = NULL)
)

nn_model3 %>% compile(
  loss = 'binary_crossentropy',
  optimizer = optimizer_sgd(lr = 0.001, momentum = 0, 
                            decay = 0, nesterov = FALSE,
                            clipnorm = NULL, clipvalue = NULL)
)

nn_model4 %>% compile(
  loss = 'binary_crossentropy',
  optimizer = optimizer_sgd(lr = 0.001, momentum = 0, 
                            decay = 0, nesterov = FALSE,
                            clipnorm = NULL, clipvalue = NULL)
)

nn_model5 %>% compile(
  loss = 'binary_crossentropy',
  optimizer = optimizer_sgd(lr = 0.001, momentum = 0, 
                            decay = 0, nesterov = FALSE,
                            clipnorm = NULL, clipvalue = NULL)
)

nn_model6 %>% compile(
  loss = 'binary_crossentropy',
  optimizer = optimizer_sgd(lr = 0.001, momentum = 0, 
                            decay = 0, nesterov = FALSE,
                            clipnorm = NULL, clipvalue = NULL)
)

nn_model7 %>% compile(
  loss = 'binary_crossentropy',
  optimizer = optimizer_sgd(lr = 0.001, momentum = 0, 
                            decay = 0, nesterov = FALSE,
                            clipnorm = NULL, clipvalue = NULL)
)

nn_model8 %>% compile(
  loss = 'binary_crossentropy',
  optimizer = optimizer_sgd(lr = 0.001, momentum = 0, 
                            decay = 0, nesterov = FALSE,
                            clipnorm = NULL, clipvalue = NULL)
)

nn_model9 %>% compile(
  loss = 'binary_crossentropy',
  optimizer = optimizer_sgd(lr = 0.001, momentum = 0, 
                            decay = 0, nesterov = FALSE,
                            clipnorm = NULL, clipvalue = NULL)
)

history1 <- nn_model1 %>% fit(
  x_train, y_train, 
  epochs = num_epochs, batch_size = 120, 
  validation_split = 0.2
)

history2 <- nn_model2 %>% fit(
  x_train, y_train, 
  epochs = num_epochs, batch_size = 120, 
  validation_split = 0.2
)

history3 <- nn_model3 %>% fit(
  x_train, y_train, 
  epochs = num_epochs, batch_size = 120, 
  validation_split = 0.2
)

history4 <- nn_model4 %>% fit(
  x_train, y_train, 
  epochs = num_epochs, batch_size = 120, 
  validation_split = 0.2
)

history5 <- nn_model5 %>% fit(
  x_train, y_train, 
  epochs = num_epochs, batch_size = 120, 
  validation_split = 0.2
)

history6 <- nn_model6 %>% fit(
  x_train, y_train, 
  epochs = num_epochs, batch_size = 120, 
  validation_split = 0.2
)

# history7 <- nn_model7 %>% fit(
#   x_train, y_train,
#   epochs = num_epochs, batch_size = 120,
#   validation_split = 0.2
# )
# 
# history8 <- nn_model8 %>% fit(
#   x_train, y_train, 
#   epochs = num_epochs, batch_size = 120, 
#   validation_split = 0.2
# )
# 
# history9 <- nn_model9 %>% fit(
#   x_train, y_train, 
#   epochs = num_epochs, batch_size = 120, 
#   validation_split = 0.2
# )


nn_model1 %>% save_model_hdf5("nn_model1.rda")
nn_model2 %>% save_model_hdf5("nn_model2.rda")
nn_model3 %>% save_model_hdf5("nn_model3.rda")
nn_model4 %>% save_model_hdf5("nn_model4.rda")
nn_model5 %>% save_model_hdf5("nn_model5.rda")
nn_model6 %>% save_model_hdf5("nn_model6.rda")

save(history1,file="history1.rdata")
save(history2,file="history2.rdata")
save(history3,file="history3.rdata")
save(history4,file="history4.rdata")
save(history5,file="history5.rdata")
save(history6,file="history6.rdata")

summary_nn <- 
  tibble(model_nr=c(1:6),
         num_hidden_layers=c(1,2,1,3,1,2),
        num_nodes=c(100,100,50,100,1000,1000),
        val_loss=c(
        mean(history1$metrics$val_loss[(num_epochs-10):num_epochs]),
            mean(history2$metrics$val_loss[(num_epochs-10):num_epochs]),
            mean(history3$metrics$val_loss[(num_epochs-10):num_epochs]),
            mean(history4$metrics$val_loss[(num_epochs-10):num_epochs]),
            mean(history5$metrics$val_loss[(num_epochs-10):num_epochs]),
            mean(history6$metrics$val_loss[(num_epochs-10):num_epochs])))

save(summary_nn,file="summary_nn.rda")


