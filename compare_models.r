
library(tidyverse)
library(glmnet)
library(keras)
library(randomForest)

# In order to calculate a binomial deviance for a random forest
# estimate, we must introduce an upper and a lower limit so that
# the prediction isn't exactly 0 or 1

source("prep_data.r",encoding = "UTF-8")

load("glmnet_model.rda")
load("RF_model.rda")
nn_model <- load_model_hdf5("nn_model5.rda")
load("history1.rdata")
load(file="summary_nn.rda")
load(file="top_important_coeff.rda")

limit_value=10^-3

limit_values <- function(vector, lower_limit=limit_value,
                         upper_limit=(1-limit_value)){
  vector <- ifelse(vector>upper_limit,upper_limit,vector)
  vector <- ifelse(vector<lower_limit,lower_limit,vector)
  return(vector)
}

test_data <- insurDataTest %>% 
  mutate(nn_pred_p=(nn_model %>% predict_proba(x_test))[,2], 
        ridge_pred_p=predict(glmnet_model, newx=X_test_matrix, 
                             type="response")[,1],
        rf_pred_p=limit_values(predict(RF_model, 
                newdata = insurDataTest, 
                type="prob")[,2]),
        y=insurDataTestClassLabels$X1,
        deviance_nn= -2 * (y * log(nn_pred_p) + (1-y) * log(1-nn_pred_p)),
        deviance_ridge= -2 * (y * log(ridge_pred_p) + (1-y) * log(1-ridge_pred_p)),
        deviance_rf= -2 * (y * log(rf_pred_p) + (1-y) * log(1-rf_pred_p))
        )

sample_size <- 800

# The task is to find sample of 800 observations that yields the
# highest sum of y. You wan't to send out 800 advertisement folders
# of caravan insurance and get the most possible new customers.
# Therefore we filter out the observations with the highest
# predicted p.

# Neural network model
num_found_nn <- test_data %>% 
  arrange(desc(nn_pred_p), .by_group = FALSE) %>%  
  filter(row_number()<=sample_size) %>%
  filter(y==1) %>%
  summarize(n())
                
# Ridge regression model
num_found_ridge <- test_data %>% 
  arrange(desc(ridge_pred_p), .by_group = FALSE) %>%  
  filter(row_number()<=sample_size) %>%
  filter(y==1) %>%
  summarize(n())

# Random foreste model
num_found_rf <- test_data %>% 
  arrange(desc(rf_pred_p), .by_group = FALSE) %>%  
  filter(row_number()<=sample_size) %>%
  filter(y==1) %>%
  summarize(n())

tot_num <- test_data %>% 
            filter(y==1) %>%
            summarize(n())

# num_found_nn / tot_num
# num_found_ridge / tot_num
# num_found_rf / tot_num

compare_pred_p <- test_data %>% 
                  select(nn_pred_p,ridge_pred_p) %>%
                  gather("p_type","p")

# ggplot(compare_pred_p, aes(x=p, group=p_type, fill=p_type)) + 
#   geom_density(alpha=.7)+
#   xlab("prediction of p") +
#   ylab("number of observations")
