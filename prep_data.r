
library(tidyverse)
library(keras)


load(file="insurDataTrain.rda")
load(file="insurDict.rda")
load(file="insurDataTest.rda")
load(file="insurDataTestClassLabels.rda")

formula <- as.factor(y) ~ .
X_train_matrix <- model.matrix(formula, data = insurDataTrain) 
y_train_frame <- model.frame(formula, data = insurDataTrain) %>% 
  model.extract("response")

insurDataTest2 <- insurDataTest %>% mutate(y=insurDataTestClassLabels$X1)
X_test_matrix <- model.matrix(formula, data = insurDataTest2) 
y_test_frame <- model.frame(formula, data = insurDataTest2) %>% 
  model.extract("response")

x_train <- insurDataTrain %>% select(-y) %>% as.matrix
y_train <- insurDataTrain %>% select(y) %>% as.matrix
y_train <- to_categorical(y_train)

x_test <- insurDataTest  %>% as.matrix
y_test <- insurDataTestClassLabels %>%  as.matrix
y_test <- to_categorical(y_test)


