Kör filerna i följande ordning:
get_and_prep_data.r
Scriptet laddar ned och gör iordning data som sedan sparas som .rda-filer

load_data.r
Laddar .rda-datafilerna och gör iordning dem på det format som 
pasar för de olika modellerna

ridge.r
Utför ridge regression och sparar modellen i en .rda-fil

neural_network.r
Utför anpassning till neural network och sparar modellen i en .rda-fil

random_forest.r
Anpassar random forest och sparar modellen i en .rda-fil

compare_models.r
Kör load_data.r som laddar allt indata, laddar även upp de modeller som 
sparats och gör jämförelsen av modeller.