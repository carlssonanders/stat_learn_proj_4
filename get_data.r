library(tidyverse) # For data manipulation

url2train <- "https://archive.ics.uci.edu/ml/machine-learning-databases/tic-mld/ticdata2000.txt"
url2dic <- "https://archive.ics.uci.edu/ml/machine-learning-databases/tic-mld/dictionary.txt"
url2test <- "https://archive.ics.uci.edu/ml/machine-learning-databases/tic-mld/ticeval2000.txt"
url2testClassLabels <- "https://archive.ics.uci.edu/ml/machine-learning-databases/tic-mld/tictgts2000.txt"

insurDataTrain <- read_delim(url2train, delim = "\t", col_names = FALSE)
insurDict <- read_table(url2dic, skip = 3, n_max = 86, col_names = F) %>% 
  separate(X1, c("Nr", "Name", "Description"), extra = "merge")
insurDataTest <- read_delim(url2test, delim = "\t", col_names = FALSE)
insurDataTestClassLabels <- read_delim(url2testClassLabels, delim = "\t", col_names = FALSE)

# X86 is the response variable, change it's name to "y"
colnames(insurDataTrain)[which(names(insurDataTrain) == "X86")] <- "y"

# TODO: maybe some variables should be re formatted

save(insurDataTrain,file="insurDataTrain.rda")
save(insurDict,file="insurDict.rda")
save(insurDataTest,file="insurDataTest.rda")
save(insurDataTestClassLabels,file="insurDataTestClassLabels.rda")